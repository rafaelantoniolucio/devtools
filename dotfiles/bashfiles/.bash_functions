shopt -s globstar
for file in /opt/devtools/lib/*.sh; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# Installing ZZ Functions (www.funcoeszz.net)
#export ZZOFF=""  # turning of not disired functions
export ZZPATH="/opt/devtools/lib/funcoeszz.sh"  # script