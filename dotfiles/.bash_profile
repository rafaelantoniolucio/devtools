# load generic bashrc
source /opt/devtools/dotfiles/.bashrc

# source the users bashrc if it exists
if [ -f "${HOME}/.bashrc" ] ; then
	source "${HOME}/.bashrc"
fi
ud
#----------------------------------------------------------------
# DASHBOARD
#----------------------------------------------------------------
# display-messages.sh;
jira -issues
echo ""
motd.sh;
echo ""
slacktime -l

# get git branch if the directory is a git repository
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/[\1]/'
}
# set the command execution status
PS1='\n\[\033]0\w\007\]\[\]\[\e[36m\][$(if [[ $? == 0 ]]; then echo "\[\e[32m\]\342\234\223"; else echo "\[\e[31m\]\342\234\227"; fi)\[\e[36m\]]\[\e[33m\]\e[m\]'
# set user at hostname in directory
PS1+=" \[\e[33m\]\u\[\e[m\] at \[\e[33m\]@\[\e[m\]\[\e[33m\]\h\[\e[m\] in \[\e[32m\]\w\[\e[m\]"
# directory size (removed because is to slow)
#PS1+=" \[\e[30;1m\](\$(/bin/ls -lah | /bin/grep -m 1 total | /bin/sed 's/total //'))"
# Print git branch
PS1+=" \[\e[35;40m\]\`parse_git_branch\`\[\e[m\] \[\e[m\]\n→ "
export PS1

git config --global core.excludesfile /opt/devtools/config/git/VisualStudio.gitignore
git config credential.helper store