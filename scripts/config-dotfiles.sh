#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}config-dotfiles.sh${CRESET}
# Create symbolics links of dotfiles and dot directories to $HOME
# Usage: ${COLOR_SCRIPT}config-dotfiles.sh${CRESET}
# Aliases: config-dotfiles
#
# Author: Diego Schmidt
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/utils.sh

function mainConfigDotfiles() {
		# Removing broken links
	find -L ${HOME} -maxdepth 2 -type l -delete

	dotfilespath='/opt/devtools/dotfiles'

	infoh "config-dotfiles -> Linking Files:"
	for file in `find $dotfilespath -maxdepth 1 -path $dotfilespath/.bashrc -prune -o -name ".*" -print`; do
		local name=${file##*/}

		if [ "${name}" != "." ] && [ "${name}" != ".." ]; then
			if [ -f "${HOME}/${name}" ] || [ -d "${HOME}/${name}" ]; then
				rm -rf "${HOME}/${name}";
			fi
			info "Linking ${file} TO ${HOME}/${name}"
			ln -sf "${file}" "${HOME}/${name}"
			successln "Linked ${file} TO ${HOME}/${name}"
		fi
	done;
	successln "All dotfiles configured."
	echo ;

	unset file;
	infoh ".bash_profile:"
	source /opt/devtools/dotfiles/.bash_profile

}

mainConfigDotfiles;
unset mainConfigDotfiles;
