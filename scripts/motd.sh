#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}motd.sh${CRESET}
# Show message of the day using ${DEVTOOLS_DATA}/motd.txt
# Usage: ${COLOR_SCRIPT}motd.sh${CRESET}
# Aliases: motd
#
# Author: Luiz H. Rapatão
# Since: 2014-10-01
#===================================================================

source /opt/devtools/lib/utils.sh;


printf ${YELLOW}'  Message of the day\n'${CRESET}
# Pega frase aleatoria
v=$(shuf -n 1 "${DEVTOOLS_DATA}/motd.txt")
# conta frase sem codigo de cores
qtd=$( echo ${v%(*} | sed 's/${[A-Z]*}//g' | wc -c )
# evalua frase para gerar sequencia de cores
v=$(eval echo -e "\"${v}\"")
#pega frase
frase="${v%(*}";
#pega autor
author="(${v#*\(}";

printf "\"%s\"\n%s\n" "${frase}" "${author}"