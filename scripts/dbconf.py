__author__ = '899297'
import sys

import ConfigParser
import optparse
import os


######################################################################################################
# Helper functions
######################################################################################################
def parse_args():
    usage = "usage: %prog [options] <country> [<environment>]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('--variable-file', dest="variable_file", help="File with variable list.", metavar="FILE")
    parser.add_option('--datasource-file', dest="datasource_file", help="File with datasource list.", metavar="FILE")
    parser.add_option('--color', dest="color", default=False, action="store_true", help="Print with bash color variables")

    opt, args = parser.parse_args(sys.argv)
    # log.debug("parsing result: %s", str(opt))

    if opt.variable_file is None or opt.variable_file == "" or opt.datasource_file is None or opt.datasource_file == "":
        print('variable_file and datasource_file are required!!!')
        print parser.print_help()
        sys.exit(1)

    if len(args) < 2:
        print('Argument <country> is required!!!\n')
        print(parser.print_help())
        sys.exit(1)

    return opt, args

def properties_to_map(properties_inline):
    prop_map = {}
    if properties_inline is not None:
        properties = properties_inline.split(",")
        for prop in properties:
            props = prop.split("=")
            prop_map[props[0]] = props[1]
    return prop_map


def get_config_section_map(conf, section):
    dict1 = {}
    if conf.has_section(section):
        sec_value = conf.options(section)
        for option in sec_value:
            dict1[option] = conf.get(section, option)

    return dict1

def populate_symbols(opt, variable_sections=None):
    symbol_map = {}

    # Adding variables from file, filtered by sections
    if opt.variable_file is not None:
        conf = ConfigParser.ConfigParser()
        conf.optionxform = str
        conf.readfp(open(opt.variable_file))

        if variable_sections is not None:
            sections = variable_sections.split(",")
        else:
            sections = conf.sections()

        for section in sections:
            symbol_map.update(get_config_section_map(conf, section))

    return symbol_map, conf

def get_ds_sections(args):
    sections = "default"
    if len(args) > 2:
        sections += "-" + args[2].upper() + "," + args[1].upper() + "-" + args[2].upper()
    else:
        sections += "," + args[1].upper()
    return sections

def parse_property(value, map_symbols):
    new_value = value
    for k, v in map_symbols.items():
        new_value = new_value.replace('${' + k + '}', v)
    return new_value

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

######################################################################################################
# Script Entry
######################################################################################################
options, arguments = parse_args()

if not os.path.exists(options.variable_file):
    print '[ERROR] File %s doesnt exists' % options.variable_file
    sys.exit(1)

if not os.path.exists(options.datasource_file):
    print '[ERROR] File %s doesnt exists' % options.datasource_file
    sys.exit(1)



ds_sections = get_ds_sections(arguments)
symbols, confSectionsDs = populate_symbols(options, ds_sections)

for sec in ds_sections.split(','):
    if not confSectionsDs.has_section(sec):
        print('There is no value for [%s]' % sec)
        sys.exit(1)

config = ConfigParser.ConfigParser()
config.optionxform = str
config.readfp(open(options.datasource_file))

green = ''
cyan = ''
reset = ''

if options.color:
    green = '${BGREEN}'
    cyan = '${BCYAN}'
    reset = '${CRESET}'

printed = []
for section in config.sections():
    opts = config.options(section)
    jndi_name = parse_property(config.get(section, 'jndi-name'), symbols).replace("$", "\$")
    host = parse_property(config.get(section, 'host'), symbols).replace("$", "\$")
    port = parse_property(config.get(section, 'port'), symbols).replace("$", "\$")
    name = parse_property(config.get(section, 'database-name'), symbols).replace("$", "\$")
    user = parse_property(config.get(section, 'user'), symbols).replace("$", "\$")
    passwd = parse_property(config.get(section, 'password'), symbols).replace("$", "\$")

    temp = 'jdbc:oracle:thin:%s:%s:%s,' % (host, port, name)
    temp += user + ','
    temp += passwd

    if temp in printed:
        continue
    printed.append(temp)

    txt = '\\\\n%s# [%s]%s\\\\n' % (green, section, reset)
    txt += 'jndi-name=%s%s%s\\\\n' % (cyan, jndi_name, reset)
    txt += 'jdbc.driverClassName=%soracle.jdbc.OracleDriver%s\\\\n' % (cyan, reset)
    txt += 'jdbc.url=%sjdbc:oracle:thin:@//%s:%s/%s%s\\\\n' % (cyan, host, port, name, reset)
    txt += 'jdbc.username=%s%s%s\\\\n' % (cyan, user, reset)
    txt += 'jdbc.password=%s%s%s\\\\n\\\\n' % (cyan, passwd, reset)
    print(txt)
