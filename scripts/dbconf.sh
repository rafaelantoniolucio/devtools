#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}dbconf.sh${CRESET}
# Show database connection data
# Usage: ${COLOR_SCRIPT}dbconf${CRESET} <br|co|mx|pa|pe> [qa|cdev|ctest]
# Aliases:
#
# Author: Diego Schmidt
# Since: 2015-08-12
#===================================================================
source /opt/devtools/lib/utils.sh;

function _dbconf() {
    if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}";
		return 1;
	fi;

	if [[ ${1} == "-h" || ${1} == "--help" ]]; then
        docScript "${BASH_SOURCE[0]}";
		return 1;
	fi

    local varFile="${HOME}/projects/devtools-deps/continuous-integration/continuous_configuration/developer/developer-ds-variables.properties";
    local dsFile="${HOME}/projects/devtools-deps/continuous-integration/continuous_configuration/developer/developer-ds.properties";
    eval echo -e $(python /opt/devtools/scripts/dbconf.py --datasource-file=${dsFile} --variable-file=${varFile} --color ${@});
}
_dbconf $@;
unset _dbconf;