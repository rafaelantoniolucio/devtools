#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}switch.sh${CRESET}
# Switch application version
# Usage: ${COLOR_SCRIPT}switch.sh${CRESET} <application> <newVersion>
# Aliases: switch
#
# Author: Diego Schmidt
# Since: 2014-08-18
#===================================================================
source /opt/devtools/lib/utils.sh;

function _mainSwitchApp() {
	if [[ ${#@} -lt 2 ]]; then
		docScript ${BASH_SOURCE[0]};
		return 1;
	fi;

	# TODO

}

_mainSwitchApp $@;
unset _mainSwitchApp;