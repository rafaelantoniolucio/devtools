#!/bin/bash
#===================================================================
# ${COLOR_LIB}utils.sh${CRESET}
# Lib defining utilities functions
#
# Author: Weverton Bernardes
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/colors.sh

# ----------------------------------------------------------------------------
# Print Functions
# ----------------------------------------------------------------------------
function infoh() {
	printf "\r\033[2K ${YELLOW} $* ${CRESET}\n"
}

function info() {
	printf "\r\033[2K  [ ${CYAN}..${CRESET} ] $*"
}

function infoln() {
	info "$*\n"
}

function news() {
	printf "\r\033[2K  [${BYELLOW}NEWS${CRESET}] $*\n"
}

function fail() {
	printf "\r\033[2K  [${RED}FAIL${CRESET}] $*\n";
}

function print() {
	printf "\r\033[2K$*";
}

function println() {
	printf "\r\033[2K  $*\n";
}

function println_on() {
	printf "\r\033[2K  [${GREEN}ON${CRESET} ] $*\n"
}

function println_off() {
	printf "\r\033[2K  [${RED}OFF${CRESET}] $*\n";
}

function println_started() {
	printf "\r\033[2K  [${GREEN}STARTED${CRESET}] $*\n"
}

function println_stopped() {
	printf "\r\033[2K  [${RED}STOPPED${CRESET}] $*\n";
}

#
function printlns() {
	printf "\r\033[2K         $*\n";
}

function success() {
	printf "\r\033[2K  [ ${GREEN}OK${CRESET} ] $*"
}

function successln() {
	success "$*\n"
}

function warn() {
	printf "\r\033[2K  [${YELLOW}WARN${CRESET}] $*"
}

function warnln() {
	warn "$*\n"
}

function notify() {
	if [[ $(command -v notifu) ]]; then
		cmd /c start notifu /w /d 5 "$@"
	fi;
}

function notify_info() {
	notify /t info /p "$1" /m "$2"
}

function notify_warn() {
	notify /t warn /p "$1" /m "$2"
}

function notify_error() {
	notify /t error /p "$1" /m "$2"
}

# ----------------------------------------------------------------------------
# Read Functions
# ----------------------------------------------------------------------------
function user() {
	printf "\r  [ ${YELLOW}?${CRESET} ] $*\n"
}

function ptquestion() {
	printf "\r  [ ${YELLOW}?${CRESET} ] $*"
}

function ptoption() {
	printf "\r\033[2K  [ ${YELLOW}$1${CRESET} ] $2\n"
}

# ----------------------------------------------------------------------------
# Logger Functions
# ----------------------------------------------------------------------------
function logger() {

	if [[ ! -d "/var/log" ]]; then
		mkdir -p /var/log
	fi

	[ ${#@} -eq 1 ] && {
		echo "${1//\.log}-$(date +%Y%m%d).log";
	}

	logfile="/var/log/${1}";
	shift;

	echo "$( date +'%Y-%m-%d %H:%M:%S' ) $@" >>${logfile};
}


#
# a) function settitle
function settitle () {
	echo -ne "\e]2;$@\a\e]1;$@\a";
}

#---
# ${COLOR_FUNC}cd_func${CRESET}
# This function defines a 'cd' replacement function capable of keeping,
#              displaying and accessing history of visited directories, up to 10 entries.
# Usage: ${COLOR_FUNC}cd${CRESET} --
#
# Author: Weverton Bernardes
# Since: 2014-08-11
#---
function cd_func() {
	local x2 the_new_dir adir index
	local -i cnt

	if [[ $1 ==  "--" ]]; then
		dirs -v
		return 0
	fi

	the_new_dir=$1
	[[ -z $1 ]] && the_new_dir=$HOME

	if [[ ${the_new_dir:0:1} == '-' ]]; then
		#
		# Extract dir N from dirs
		index=${the_new_dir:1}
		[[ -z $index ]] && index=1
		adir=$(dirs +$index)
		[[ -z $adir ]] && return 1
		the_new_dir=$adir
	fi

	#
	# '~' has to be substituted by ${HOME}
	[[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"

	#
	# Now change to the new dir and add to the top of the stack
	pushd "${the_new_dir}" > /dev/null
	[[ $? -ne 0 ]] && return 1
	the_new_dir=$(pwd)

	#
	# Trim down everything beyond 11th entry
	popd -n +11 2>/dev/null 1>/dev/null

	#
	# Remove any other occurence of this dir, skipping the top of the stack
	for ((cnt=1; cnt <= 10; cnt++)); do
		x2=$(dirs +${cnt} 2>/dev/null)
			[[ $? -ne 0 ]] && return 0
		[[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
		if [[ "${x2}" == "${the_new_dir}" ]]; then
			popd -n +$cnt 2>/dev/null 1>/dev/null
			cnt=cnt-1
		fi
	done

	return 0
}

#---
# ${COLOR_FUNC}todo${CRESET}
# Create a text file to the Desktop
# Usage: ${COLOR_FUNC}todo${CRESET} <text>
#
# Author: Weverton Bernardes
# Since: 2014-08-11
#---
function todo() {
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" todo;
		return 1;
	fi;
	touch ${USERPROFILE}/Desktop/"$*".todo
}


function isCygwin() {
	isOS "cygwin";
}

function isOS() {
	local os=${1^^};
	[ -z "${MYSYSTEM}" ] && {
		local sys=$( uname -s );
		sys=${sys^^};
		MYSYSTEM=${sys%_*};
	}
	[ "${os}" == "${MYSYSTEM}" ]
}

#---
# ${COLOR_FUNC}ps${CRESET}
# Replace cygwin ps and include MS compatibility
# Usage: ${COLOR_FUNC}ps${CRESET}
#
# Author: Pedro Leao
# Since: 2014-08-11
#---
function ps() {
  set -- ${@/-/-W};
  /bin/ps ${@};
}

#---
# ${COLOR_FUNC}kill${CRESET}
# Replace cygwin ps and include MS compatibility
# Usage: ${COLOR_FUNC}kill${CRESET}
#
# Author: Pedro Leao
# Since: 2014-08-11
#---
function kill() {
  local parm=${@};
  /bin/kill ${parm} 2>/dev/null || {
	local task=;
	for task in  ${parm##*-[0-9] } ; do
	  tskill ${task};
	done
  }
}

#---
# ${COLOR_FUNC}usage${CRESET}
# Show all functions comments
# Usage: ${COLOR_FUNC}usage${CRESET} <file_name> [function_name]
# Ex: usage /opt/devtools/lib/utils.sh
#     usage /opt/devtools/lib/utils.sh usage
#
# Author: Luiz Rapatao && Pedro Leao
# Since: 2014-08-11
#---
function usage() {
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" usage;
		return 1;
	fi;

	if [[ ${2} != "" ]]; then
		local CMD="${2}\(\)";

		lines=( $( egrep -n "(^#---|${CMD})" ${1} | egrep -B2 "${CMD}" | cut -d : -f 1) );

		local start=${lines[0]};
		local end=${lines[1]};

		[ ! -z "${start}" ] && [ ! -z "${end}" ] && {
			local nl=$((end-start));
			local txt=$( head -n +$((end-1)) ${1} | tail -${nl} | sed -e "s/^#//g; s/-*//; s/^/ /" | applyColorTo "${CMD} " ${COLOR_FUNC});
			eval echo -e "\"${txt}\""
		}
	else
		echo;
		local txt=$(tail -n +2 ${1} | egrep "Usage[\$\{\}a-zA-Z]*:" | sed -e "s/^#//g; s/^/ /" | sort);
		eval echo -e "\"${txt}\""
	fi;
}

#---
# ${COLOR_FUNC}docScript${CRESET}
# Show script doc
# Usage: ${COLOR_FUNC}docScript${CRESET} <script_file_name>
# Ex: usage /opt/devtools/lib/utils.sh
#
# Author: Weverton Bernardes
# Since: 2014-08-11
#---
function docScript() {
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" docScript;
		return 1;
	fi;

	if [[ ${1} != "" ]]; then
		lines=( $( egrep -n "(^#===)" ${1} | cut -d : -f 1) );

		local start=${lines[0]};
		local end=${lines[1]};

		[ ! -z "${start}" ] && [ ! -z "${end}" ] && {
			local nl=$((end-start));

			local txt=$( head -n +$((end-1)) ${1} | tail -${nl} | sed -e "s/^#//g; s/=*//; s/^/ /" | applyColorTo "${CMD} " ${COLOR_FUNC});
			eval echo -e "\"${txt}\""
		}
	# else
	# 	echo;
	# 	printf "$(tail -n +2 ${1} | egrep "^# ${COLOR_FUNC}Usage${CRESET}:" | sed -ne "s/^# \(${COLOR_FUNC}Usage${CRESET}:\)/  ${CYAN//\\/\\\\}\1${RESET//\\/\\\\}/; /${COLOR_FUNC}Usage${CRESET}:/p" | sort)";
	# 	echo;
	fi;
}

#---
# ${COLOR_FUNC}applyColorTo${CRESET}
# Apply color to part of the text
# Usage: ${COLOR_FUNC}applyColorTo${CRESET} <pattern> <color> <input>
# Ex: applyColorTo "Usage" "'${COLOR_FUNC}'"
#
# Author: Weverton Bernardes
# Since: 2014-08-12
#---
function applyColorTo() {
	if [[ ${#@} -lt 2 ]]; then
		usage "${BASH_SOURCE[0]}" applyColorTo;
		return 1;
	fi;
	# Destaca o padrão $1 no texto via STDIN ou $2
	# O padrão pode ser uma regex no formato BRE (grep/sed)
	local reset=$(printf "$CRESET");
	local cor=$(printf "${2}");
	local padrao=$(echo "$1" | sed 's,/,\\/,g'); # escapa /
	shift 2;
	_multi_stdin "$@" |
		sed "s/$padrao/${cor}&${reset}/g";
}

function _multi_stdin() {
	# Mostra na tela os argumentos *ou* a STDIN, nesta ordem
	# Útil para funções/comandos aceitarem dados das duas formas:
	#     echo texto | funcao
	# ou
	#     funcao texto

	if [ "$1" ]
	then
		echo "$*"  # security: always quote to avoid shell expansion
	else
		cat -
	fi
}

#---
# ${COLOR_FUNC}capitalizeEachWord${CRESET}
# Capitalize the first letter of each word and leave the other letters lowercase
# Usage: ${COLOR_FUNC}capitalizeEachWord${CRESET} <phrase>
# Ex: capitalizeEachWord "phrase"
#
# Author: Luiz Rapatao
# Since: 2014-08-14
#---
function capitalizeEachWord() {
	echo ${@} | sed -e "s/[ ]*\b\(.\)/\u\1/g";
}

function getRealPath() {
	if [[ isCygwin ]]; then
		echo "$(cygpath -m ${1})";
	else
		echo "${1}";
	fi;
}

#---
# ${COLOR_FUNC}xor${CRESET}
# Decrypt Websphere passwords
# Usage: ${COLOR_FUNC}xor${CRESET} <encrypted password> or use - to read stdin
# Ex: xor '{xor}Lj58bWxuPg=='
#
# Author: Weverton Bernardes
# Since: 2015-03-18
#---
function xor(){
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" xor;
		return 1;
	fi;

    plaintext="${1}";

	[ "${plaintext}" == "-" ] && {
		read plaintext;
	}

    plaintext=${plaintext//\{xor\}/};
    plaintext=$( echo "${plaintext}" | base64 -d );

	ciphertext=""
	for ((i=0; i < ${#plaintext}; i++ ))
	do
	   ord=$(printf "%d" "'${plaintext:$i:1}")
	   tmp=$(printf \\$(printf '%03o' $((ord ^ 95)) ))
	   ciphertext="${ciphertext}${tmp}"
	done
	echo "$ciphertext"
}

#---
# ${COLOR_FUNC}getheader${CRESET}
# Prepare class Header's
# Usage: ${COLOR_FUNC}getheader${CRESET} <class name> <description of the class>
# Ex: getheader 'Class1.cs' 'The firs class of the project'
#
# Author: Weverton Bernardes
# Since: 2015-03-18
#---
function getheader(){
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" getheader;
		return 1;
	fi;
	echo "";
	echo "";
    cat /opt/devtools/data/classHeaders.txt | sed 's/<Class Name>/'"${1}"'/g' | sed 's/<Description>/'"${2}"'/g' | sed 's/<Author>/'$(echo $USER)'/g' | sed 's/<Date>/'$(date +"%d-%m-%Y")'/g'
	echo "";
	echo "";
	 
}

#---
# ${COLOR_FUNC}ochrome${CRESET}
# Open 10 first google search results in chrome
# Usage: ${COLOR_FUNC}ochrome${CRESET} word or phrease
# Ex: ochrome stackoverflow
#
# Author: Weverton Bernardes
# Since: 2016-13-28
#---
function ochrome(){
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" ochrome;
		return 1;
	fi;
	chrome $(zzgoogle -n 10 "${@}" | grep -i http)
	 
}

#---
# ${COLOR_FUNC}ochrome${CRESET}
# Open 10 first google search results in chrome incognito
# Usage: ${COLOR_FUNC}ochrome${CRESET} word or phrease
# Ex: ochrome stackoverflow
#
# Author: Weverton Bernardes
# Since: 2016-13-28
#---
function ochromei(){
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" ochrome;
		return 1;
	fi;
	chromei $(zzgoogle -n 10 "${@}" | grep -i http)
}

#---
# ${COLOR_FUNC}jira${CRESET}
# Execute some functions in jira
# Usage: ${COLOR_FUNC}jira${CRESET} -option [p, i, c, s, h]
# Ex: jira -h (to view help)
#
# Author: Weverton Bernardes
# Since: 2016-1-23
#---
function jira(){
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" jira;
		return 1;
	fi
		
		case ${1} in
			-p|-projects) jracons -projects ;;
			-i|-issues) jracons -issues ;;		
			-c|-config) jracons -configure ${2} ${3} ${4};;		
			-s|-search) chrome $(echo "https://easynvest.atlassian.net/issues/?jql=${@}" | sed 's/'$1'\ //g') ;;		
			[?]) 
				echo "final"
				;;
		esac
}

#---
# ${COLOR_FUNC}stack${CRESET}
# seach in stackoverflow
# Usage: ${COLOR_FUNC}stack${CRESET} word or phrase
# Ex: stack some error (to view help)
#
# Author: Weverton Bernardes
# Since: 2016-2-10
#---
function stack(){
	if [[ ${#@} -lt 1 ]]; then
		usage "${BASH_SOURCE[0]}" stack;
		return 1;
	fi
	chrome "$(echo "http://stackoverflow.com/search?q=${@}")" ;		
		
}

   

